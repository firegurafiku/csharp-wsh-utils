/**
 * @file SignTargetFiles.js
 * @title Signing target output executables or libraries
 *
 * This script was written for using in PostBuild event of Visual C# projects
 * to simplify executable and library files signing.
 *
 * @section synopsis Synopsis
 * @code
 *     cscript SignOutputFiles.js KEYFILE.pfx TARGETNAME TARGETDIR
 * @endcode
 * where parameters are:
 *     KEYFILE.pfx Private key container saved without password.
 *     TARGETNAME  Target file name without extension (usually project name).
 *     TARGETDIR   Directory where TARGETNAME.exe or .dll resides.
 *
 * @section desc Description
 * Script tries to find and sign with given key file (using @c signtool.exe
 * program which comes out of box with Visual Studio and must be in PATH)
 * the following files in TARGETDIR folder:
 *
 *   - @c TARGETNAME.exe
 *   - @c TARGETNAME.dll
 *   - @c *\TARGETNAME.resources.dll
 *
 * @section authors Authors and License
 * First version was written by Pavel Kretov (firegurafiku@gmail.com)
 * in October of 2012. It is licensed under the terms of MIT license.
 */

if (WScript.Arguments.length < 3)
{
    WScript.Echo("Error: Too few arguments in command line.");
    WScript.Quit(1);
}

var keyFile    = WScript.Arguments.Unnamed(0);
var targetName = WScript.Arguments.Unnamed(1);
var targetDir  = WScript.Arguments.Unnamed(2);

var fileSystem = WScript.CreateObject("Scripting.FileSystemObject");

if (!fileSystem.FileExists(keyFile))
{
    WScript.Echo("Error: Key file doesn't exist.");
    WScript.Quit(1);
}

if (!fileSystem.FolderExists(targetDir))
{
    WScript.Echo("Error: Target directory doesn't exist.");
    WScript.Quit(1);
}

var extensionsToSign = [ ".exe", ".dll" ];
var filesToSign = [];

// Test if TARGETNAME.exe or TARGETNAME.dll exists in TARGETDIR. If such files
// exist, add them to the list of files scheduled for sign.
var extEnum = new Enumerator(extensionsToSign);
for (extEnum.moveFirst(); !extEnum.atEnd(); extEnum.moveNext())
{
    var extension = extEnum.item();;
    var fileName = targetDir + "\\" + targetName + extension;
    if (fileSystem.FileExists(fileName))
    {
        filesToSign[filesToSign.length] = fileName;
    }
}

// Test if TARGETNAME.resources.dll exists in the subfolders of TARGETDIR,
// but not deeper. If such files exist, add them to the list of files scheduled
// for sign.
var dirEnum = new Enumerator(fileSystem.GetFolder(targetDir).SubFolders);
for (dirEnum.moveFirst(); !dirEnum.atEnd(); dirEnum.moveNext())
{
    var extEnum = new Enumerator(extensionsToSign);
    for (extEnum.moveFirst(); !extEnum.atEnd(); extEnum.moveNext())
    {
        var directory = dirEnum.item();
        var extension = extEnum.item();
        var fileName = directory + "\\" + targetName + ".resources" + extension;
        if (fileSystem.FileExists(fileName))
        {
            filesToSign[filesToSign.length] = fileName;
        }
    }
}

// Form the command line to start signtool.exe and escape its arguments with
// double quotes.
var cmdProgram = "signtool.exe";
var cmdOptions = "sign /f " + "\"" + keyFile + "\"";
var cmdArguments = "";
for (var fileIdx in filesToSign)
{
    cmdArguments += "\"" + filesToSign[fileIdx] + "\" ";
}

// Run signtool to sign all found files at once. But this may fail if KEYFILE
// has set a password on it. Or if files list is too long (unlikely).
var cmdLine = cmdProgram + " " + cmdOptions + " " + cmdArguments;

var signtool;
try
{
    var shell = WScript.CreateObject("WScript.Shell")
    signtool = shell.Exec(cmdLine);
}
catch (exception)
{
    WScript.Echo("Error: Unable to start signtool.");
    WScript.Quit(1);
}

// Wait for signtool to exit.
while (signtool.Status != 1)
{
    WScript.Sleep(0);
}

// Print the error if something went wrong.
if (signtool.ExitCode != 0)
{
    WScript.Echo("Error: Signtool exited with error.");
    WScript.Quit(1);
}
