Helper scripts for C# projects in Visual Studio Express
=======================================================
**Author:** [Pavel Kretov](mailto:firegurafiku@gmail.com)  
**License:** [WTFPL of any version](http://en.wikipedia.org/wiki/WTFPL)  
**Language:** JavaScript

These scripts were useful for me when I was working with Microsoft Visual C# Express
Edition which lacks support for some features like template based code generation (it
is why JsTemplate.js was created). Being written in [WSH][1], they
are not very usable outside of Microsoft's world.

[1]: http://en.wikipedia.org/wiki/Windows_Script_Host