/**
 * @file JsTemplate.js
 * @title Simple source code templating with Windows Script Host JavaScript.
 *
 * @section S Synopsis
 * Use the following invocation syntax:
 * @code
 *     cscript JsTemplate.js PROJECT_DIR
 * @endcode
 * where @c PROJECT_DIR is the path to directory for template files recursive
 * search. All files which names are <tt>*.jstt.*</tt> are treated as template
 * files. Such behavior seems to be quite strange for command line tools in
 * Unix-like systems, but the script was designed to work in Windows with
 * Visual Studio projects, to be able to run from PreBuild event. So, that's
 * okay for such an ad-hoc solution.
 *
 * @section D Description
 * This script was originally designed for preprocessing of C# code files,
 * but can be easily used for any language with similar syntax.
 *
 * To use this script you should name your template file with @c jstt infix,
 * which will be omitted in the result file name. For example, processing
 * file @c MatrixTemplate.jstt.cs will generate @c MatrixTemplate.cs file.
 * Use //# markers to write JavaScript template code, everything without
 * such "comments" are treated like verbatim output. For example,
 * @code
 *     //# for (i=0; i<5; ++i) {
 *     print("Hello WORLD!");
 *     //# }
 * @endcode
 * will output second line five times.
 *
 * @warning
 * This script does NOT do all necessary syntax parsing. Please avoid using
 * marker <tt>//#</tt> anywhere else in your code but to designate JavaScript
 * template code blocks. Do not use it even in comments and string literals.
 *
 * @section F Functions
 * You may use any built-in JavaScript function in template code. Also, there
 * are some special functions available, provided in order to make common
 * preprocessing tasks easier. The complete functions list follows:
 *
 * @c Output(str)
 * Outputs given text @a str to generated file, no newline added.
 *
 * @c OutputLine(str)
 * Outputs given text @a str to generated file, with newline added.
 *
 * @c PasteFile(fileName)
 * Outputs file content to generated file, line by line. File considered as
 * raw strings, no template processing is done.
 *
 * @c PasteFilePart(fileName, cutMarker, endMarker)
 * Acts like @c PasteFile, but outputs only the part of file delimited by
 * marker lines. If no marker lines given, "=== CUT ===" and "=== END ==="
 * are used as default values.
 *
 * @section AL Authors and License
 * First version was written by Pavel Kretov (firegurafiku@gmail.com)
 * in January of 2013. It is licensed under the terms of MIT license.
 */

/* --- scope for eval() --- */

// Global variables for internal use in client functions. Generally
// speaking, you must not use in your template script anything which
// name starts from double underscrore. But there is no way to truly
// hide that variables from caller.
var __fileSystem = WScript.CreateObject("Scripting.FileSystemObject");
var __currentDirectory = "";
var __sourceFile = "";
var __targetFile = "";
var __targetStream = null;
var __replacesGuard = 0;
var __replaces = [];
var __replacesNextLine = [];
var __replacesAll = [];

function __multipleReplaceRev(str, replacements)
{
    var result = "";
    var rest = str;

    while (rest != "")
    {
        var closestMatch = null;
        var closestIdx;
        for (var idx = replacements.length-1; idx >= 0; --idx)
        {
            var rex  = replacements[idx][0];
            var repl = replacements[idx][1];
            var match = rex.exec(rest);

            if (match && match.index >= 0)
            {
                if (!closestMatch || match.index < closestMatch.index)
                {
                    closestMatch = match;
                    closestIdx = idx;
                }
            }
        }

        if (!closestMatch)
        {
            result += rest;
            rest = "";
        }
        else
        {
            var matchIndex = closestMatch.index;
            var matchLength = closestMatch[0].length;

            result += rest.substr(0, matchIndex);
            result += closestMatch[0].replace(
                replacements[closestIdx][0],
                replacements[closestIdx][1]);

            rest = rest.substr(matchIndex + matchLength);
        }
    }

    return result;
}

function __applyReplacements(str, nextLine)
{
    var result = str;
    if (!__replacesGuard)
    {
        result = __multipleReplaceRev(result, nextLine
            ? __replacesAll
            : __replaces);
    }

    return result;
}

function __output(str)
{
    Output(__applyReplacements(str, false));
}

function __outputLine(line)
{
    OutputLine(__applyReplacements(line, true));
}

function __wrapIdentifier(name)
{
    // FIXME: More sofisticated regex required to test itentifier borders.
    // FIXME: Check name argument before pasting into regex.
    return new RegExp('\\b' + name + '\\b');
}

function Output(str)
{
    __targetStream.Write(str);
}

function OutputLine(line)
{
    __targetStream.WriteLine(line);
}

function ReplaceInNextLine(rex, replace)
{
    var tuple = [rex, replace];
    __replacesNextLine.push(tuple);
    __replacesAll.push(tuple);
}

function ReplaceIdentifierInNextLine(name, replace)
{
    var tuple = [__wrapIdentifier(name), replace];
    __replacesNextLine.push(tuple);
    __replacesAll.push(tuple);
}

function PushReplace(rex, replace)
{
    __replaces.push([rex, replace]);
    __replacesAll = __replaces.concat(__replacesNextLine);
}

function PushReplaceIdentifier(name, replace)
{
    __replaces.push([__wrapIdentifier(name), replace]);
    __replacesAll = __replaces.concat(__replacesNextLine);
}

function PopReplace()
{
    __replaces.pop();
    __replacesAll = __replaces.concat(__replacesNextLine);
}

function DisableReplaces()
{
    __replacesGuard++;
}

function EnableReplaces()
{
    __replacesGuard--;
}

function ResetReplacesInNextLine()
{
    __replacesNextLine = [];
    __replacesAll = __replaces.concat(__replacesNextLine);
}

function ResetReplaces()
{
    __replaces = [];
    __replacesNextLine = [];
    __replacesAll = [];
    __replacesGuard = 0;
}

function ParseJsonFile(fileName)
{
    var fullFileName = __fileSystem.BuildPath(
        __fileSystem.GetParentFolderName(__sourceFile),
        fileName);

    var stream;
    try
    {
        // FIXME: Magic number "1" meaning "ForReading".
        stream = __fileSystem.OpenTextFile(fullFileName, 1);
    }
    catch (exception)
    {
        WScript.Echo(
            "Error: ParseJsonFile(): Unable to open file (%1)."
            .replace(/%1/g, fullFileName));
        return;
    }

    return eval('(' + stream.ReadAll() + ')');
}

function PasteFile(fileName)
{
    var fullFileName = __fileSystem.BuildPath(
        __fileSystem.GetParentFolderName(__sourceFile),
        fileName);

    var stream;
    try
    {
        // FIXME: Magic number "1" meaning "ForReading".
        stream = __fileSystem.OpenTextFile(fullFileName, 1);
    }
    catch (exception)
    {
        WScript.Echo(
            "Error: PasteFile(): Unable to open file (%1)."
            .replace(/%1/g, fullFileName));
        return;
    }

    while (!stream.AtEndOfStream)
    {
        OutputLine(stream.ReadLine());
    }
}

function PasteFilePart(fileName, cutMarker, endMarker)
{
    if (!cutMarker) cutMarker = "=== CUT ===";
    if (!endMarker) endMarker = "=== END ===";

    var fullFileName = __fileSystem.BuildPath(
        __fileSystem.GetParentFolderName(__sourceFile),
        fileName);

    var stream;
    try
    {
        // FIXME: Magic number "1" meaning "ForReading".
        stream = __fileSystem.OpenTextFile(fullFileName, 1);
    }
    catch (exception)
    {
        WScript.Echo(
            "Error: PasteFilePart(): Unable to open file (%1)."
            .replace(/%1/g, fullFileName));
        return;
    }

    var cut = false;
    while (!stream.AtEndOfStream)
    {
        var line = stream.ReadLine();
        if (!cut && line.indexOf(cutMarker)>=0) { cut = true; continue; }
        if (cut && line.indexOf(endMarker)>=0) { cut = false; continue; }

        if (cut)
            OutputLine(line);
    }
}

/* --- scope for main script --- */
function __main_program() {

var FileSystem = __fileSystem;

// Performs action upon all files in the given directory and subdirectories.
function foreachFile(directory, action)
{
    var folder = FileSystem.GetFolder(directory);

    var enumerator = new Enumerator(folder.Files);
    for (enumerator.moveFirst(); !enumerator.atEnd(); enumerator.moveNext())
    {
        var file = enumerator.item();
        action(file);
    }

    enumerator = new Enumerator(folder.SubFolders);
    for (enumerator.moveFirst(); !enumerator.atEnd(); enumerator.moveNext())
    {
        var subDirectory = enumerator.item();
        foreachFile(subDirectory.Path, action);
    }

    return null;
}

// Quotes line to form JavaScript string literal.
// @see http://stackoverflow.com/a/3967927
function quoteLiteralLine(line)
{
    return line
        .replace(/\\/g,'\\\\')
        .replace(/'/g,"\\'");
}

// Converts mixed template file content to pure JavaScript code
// for later running with eval().
function transformText(sourceStream)
{
    // FIXME: Improve parsing one day.
    var rex = /^(.*)\/\/#(.*)$/;
    var result = "";

    while (!sourceStream.AtEndOfStream)
    {
        var line = sourceStream.ReadLine();
        var data = line;
        var code = null;

        var match = rex.exec(line);
        if (!match)
        {
            result += "__outputLine('%1');\n"
                .replace(/%1/g, quoteLiteralLine(line))
        }
        else
        {
            var data = match[1];
            var code = match[2];
            // FIXME: Use multiple replace here. Where the hell is format() in JS?
            result += "__output('%1'); %2;\n"
                .replace(/%1/g, quoteLiteralLine(data))
                .replace(/%2/g, code);
        }
    }

    return result;
}

function processTemplateFile(sourceFile, targetFile)
{
    var sourceStream;
    var targetStream;

    try
    {
        // FIXME: Magic number "1" meaning "ForReading".
        sourceStream = FileSystem.OpenTextFile(sourceFile, 1);
    }
    catch (exception)
    {
        WScript.Echo(
            "Error: Unable to open source file (%1)."
            .replace(/%1/g, sourceFile));
        return;
    }

    try
    {
        // FIXME: Magic number "2" meaning "ForWriting".
        // FIXME: Magic boolean "true" meaning "CreateNew".
        targetStream = FileSystem.OpenTextFile(targetFile, 2, true);
    }
    catch (exception)
    {
        WScript.Echo(
            "Error: Unable to open target file (%1)."
            .replace(/%1/g, targetFile));
        return;
    }

    var rex = /^(.*)\/\/#(.*)$/;
    var transformedText = transformText(sourceStream);

    __sourceFile = sourceFile;
    __targetFile = targetFile;
    __targetStream = targetStream;
    eval(transformedText);
}

/* --- main --- */

if (WScript.Arguments.length < 1)
{
    WScript.Echo("Error: Too few arguments in command line.");
    WScript.Quit(1);
}

var projectDir = WScript.Arguments.Unnamed(0);

foreachFile(projectDir, function (file) {
    var rex = /^([^.]+)\.jstt(\.[^.]+)$/i;
    var match = rex.exec(file.Name);
    if (!match)
        return;

    var targetFileName = FileSystem.BuildPath(
        FileSystem.GetParentFolderName(file.Path),
        match[1] + match[2]);

    processTemplateFile(file.Path, targetFileName);
});

}

__main_program();

